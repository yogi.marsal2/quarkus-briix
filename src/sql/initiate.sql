create table post_tag (post_id bigint not null, tag_id bigint not null);
create table posts (id bigint not null, content TEXT, title TEXT, primary key (id));
create table tags (id bigint not null, label varchar(255), primary key (id));
alter table if exists tags drop constraint if exists UK_k5bytoqjeo5h4p9xcily5dst3;
alter table if exists tags add constraint UK_k5bytoqjeo5h4p9xcily5dst3 unique (label);
create sequence posts_SEQ start with 1 increment by 1;
create sequence tags_SEQ start with 1 increment by 1;
alter table if exists post_tag add constraint FK98d0eqovrn75s8a74oebe4sn1 foreign key (tag_id) references tags;
alter table if exists post_tag add constraint FKogo3xicgxxbhoekuj3i4aiatb foreign key (post_id) references posts;