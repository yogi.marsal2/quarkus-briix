package org.acme.exception.handler;

import org.acme.exception.APIException;
import org.acme.model.ResponseModel;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class DefaultExceptionHandler implements ExceptionMapper<APIException>{
	
	@Override
	public Response toResponse(APIException exception) {
		return Response.status(exception.getErrorCode()).entity(new ResponseModel(exception.getMessage(), true)).build();
	}
}


