package org.acme.exception;

import jakarta.ws.rs.core.Response.Status;

public class APIException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2032433913113475717L;
	Status errorCode = Status.INTERNAL_SERVER_ERROR;

	public APIException(Status errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	public Status getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Status errorCode) {
		this.errorCode = errorCode;
	}

}
