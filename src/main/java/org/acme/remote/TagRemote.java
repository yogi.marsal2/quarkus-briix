package org.acme.remote;

import java.util.ArrayList;
import java.util.List;

import org.acme.entity.TagEntity;
import org.acme.exception.APIException;
import org.acme.model.TagModel;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response.Status;

@ApplicationScoped
public class TagRemote {
	
	@Inject
	EntityManager entityManager;
	
	@Inject
	PostRemote postRemote;

	public List<TagModel> getTags() throws APIException {
		try {
			List<TagEntity> tags = entityManager.createQuery("FROM tags", TagEntity.class).getResultList();
			List<TagModel> result = new ArrayList<TagModel>();
			tags.forEach(it -> {
				result.add(entityToJson(it, false));
			});
			return result;
		} catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	public TagModel getTagDetail(long id) throws APIException {
		try {
			TagEntity tag = entityManager.find(TagEntity.class, id);
			return entityToJson(tag, true);
		} catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	@Transactional
	public TagModel createTag(TagModel model) throws APIException {
		try {
			TagEntity entity = new TagEntity();
			updateEntity(entity, model);
			entityManager.persist(entity);
			entityManager.flush();
			return entityToJson(entity, false);
		} catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	@Transactional
	public TagModel updateTag(long id, TagModel model) throws APIException {
		try {
			TagEntity entity = entityManager.find(TagEntity.class, id);
			updateEntity(entity, model);
			entityManager.merge(entity);
			entityManager.flush();
			return entityToJson(entity, false);
		} catch (Exception e) {
			e.printStackTrace();
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	@Transactional
	public List<TagEntity> saveTags(List<String> models) throws APIException {
		List<TagEntity> tags = entityManager.createQuery("FROM tags WHERE label IN (:labels)", TagEntity.class)
				.setParameter("labels", models)
				.getResultList();
		
		List<String> tempTags = new ArrayList<>();
		models.forEach(it -> {
			tempTags.add(it);
		});
		
		tags.forEach(it -> {
			tempTags.remove(it.getLabel());
		});
		
		tempTags.forEach(it -> {
			TagEntity entity = new TagEntity();
			entity.setLabel(it);
			entityManager.persist(entity);
			entityManager.flush();
			tags.add(entity);
		});
		
		return tags;
	}
	
	public boolean deleteTag(long id) throws APIException {
		try {
			int isSuccessful = entityManager.createQuery("DELETE FROM tags WHERE id=:id")
		            .setParameter("id", id)
		            .executeUpdate();
			return isSuccessful == 1;
		}  catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	private TagModel entityToJson(TagEntity entity, boolean isFetchPost) {
		TagModel jsonModel = new TagModel();
		jsonModel.setId(entity.getId());
		jsonModel.setLabel(entity.getLabel());
		
		if (isFetchPost) {
			entity.getPosts().forEach(it -> {
				try {
					jsonModel.getPosts().add(postRemote.getPostById(it.getId()));
				} catch (APIException e) {
					e.printStackTrace();
				}
			});
		}
		return jsonModel;
	}
	
	private void updateEntity(TagEntity entity, TagModel jsonModel) {
		entity.setLabel(jsonModel.getLabel());
	}
	
}
