package org.acme.remote;

import java.util.ArrayList;
import java.util.List;

import org.acme.entity.PostEntity;
import org.acme.entity.TagEntity;
import org.acme.exception.APIException;
import org.acme.model.PostModel;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response.Status;

@ApplicationScoped
public class PostRemote {
	
	@Inject
    EntityManager entityManager;
	
	@Inject
	TagRemote tagRemote;

	public List<PostModel> getPost() throws APIException {
		try {
			List<PostEntity> posts = entityManager.createQuery("FROM posts", PostEntity.class).getResultList();
			List<PostModel> result = new ArrayList<PostModel>();
			posts.forEach(it -> {
				result.add(entityToJson(it));
			});
			return result;
		} catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	public PostModel getPostById(long id) throws APIException {
		try {
			PostEntity post = entityManager.find(PostEntity.class, id);
			return entityToJson(post);
		} catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	@Transactional
	public PostModel createPost(PostModel model) throws APIException {
		try {
			List<TagEntity> tags = tagRemote.saveTags(model.getTags());
			PostEntity entity = new PostEntity();
			updateEntity(entity, model, tags);
			entityManager.persist(entity);
			entityManager.flush();
			return entityToJson(entity);
		} catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	@Transactional
	public PostModel updatePost(long id, PostModel model) throws APIException {
		try {
			List<TagEntity> tags = tagRemote.saveTags(model.getTags());
			PostEntity entity = entityManager.find(PostEntity.class, id);
			updateEntity(entity, model, tags);
			entityManager.merge(entity);
			entityManager.flush();
			return entityToJson(entity);
		} catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	@Transactional
	public boolean deletePost(long id) throws APIException {
		try {
			int isSuccessful = entityManager.createQuery("DELETE FROM posts WHERE id=:id")
		            .setParameter("id", id)
		            .executeUpdate();
			return isSuccessful == 1;
		}  catch (Exception e) {
			throw new APIException(Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	private PostModel entityToJson(PostEntity entity) {
		PostModel jsonModel = new PostModel();
		jsonModel.setId(entity.getId());
		jsonModel.setTitle(entity.getTitle());
		jsonModel.setContent(entity.getContent());
		
		List<TagEntity> tags = entity.getTags();
		for (int i = 0; i < tags.size(); i++) {
			jsonModel.getTags().add(tags.get(i).getLabel());
		}
		
		return jsonModel;
	}
	
	private void updateEntity(PostEntity entity, PostModel jsonModel, List<TagEntity> tags) {
		entity.setTitle(jsonModel.getTitle());
		entity.setContent(jsonModel.getContent());
		
		tags.forEach(it -> {
			entity.getTags().add(it);
		});
	}
}
