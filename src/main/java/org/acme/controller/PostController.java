package org.acme.controller;

import java.util.List;

import org.acme.exception.APIException;
import org.acme.model.PostModel;
import org.acme.model.ResponseModel;
import org.acme.remote.PostRemote;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

@Path("api/posts")
public class PostController {
	
	@Inject
	PostRemote remote;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PostModel> getPost() throws APIException {
		return remote.getPost();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public PostModel createPost(PostModel model) throws APIException {
		return remote.createPost(model);
	}
	
	@PUT
	@Path("/{postId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public PostModel updatePost(@PathParam(value = "postId") long postId, PostModel model) throws APIException {
		return remote.updatePost(postId, model);
	}
	
	@DELETE
	@Path("/{postId}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseModel deletePost(@PathParam(value = "postId") long postId) throws APIException {
		if (remote.deletePost(postId)) {
			return new ResponseModel("Success to delete post");
		}
		
		throw new APIException(Status.BAD_REQUEST, "Failed to delete post");
	}
}
