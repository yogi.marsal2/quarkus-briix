package org.acme.controller;

import java.util.List;

import org.acme.exception.APIException;
import org.acme.model.ResponseModel;
import org.acme.model.TagModel;
import org.acme.remote.TagRemote;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;

@Path("api/tags")
public class TagController {
	
	@Inject
	TagRemote remote;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<TagModel> getTag() throws APIException {
		return remote.getTags();
	}
	
	@GET
	@Path("/{tagId}")
	@Produces(MediaType.APPLICATION_JSON)
	public TagModel getTagDetail(@PathParam(value = "tagId") long tagId) throws APIException {
		return remote.getTagDetail(tagId);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TagModel createTag(TagModel model) throws APIException {
		return remote.createTag(model);
	}
	
	@PUT
	@Path("/{tagId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TagModel updateTag(@PathParam(value = "tagId") long tagId, TagModel model) throws APIException {
		return remote.updateTag(tagId, model);
	}
	
	@DELETE
	@Path("/{tagId}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseModel deleteTag(@PathParam(value = "tagId") long tagId) throws APIException {
		if (remote.deleteTag(tagId)) {
			return new ResponseModel("Success to delete tag");
		}
		
		throw new APIException(Status.BAD_REQUEST, "Failed to delete post");
	}
	
}
