package org.acme.model;

public class ResponseModel {

	private String message;
	private boolean error;
	
	public ResponseModel() {
		super();
		this.message = "";
		this.error = false;
	}
	
	public ResponseModel(String message) {
		super();
		this.message = message;
		this.error = false;
	}
	
	public ResponseModel(String message, boolean error) {
		super();
		this.message = message;
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
	
}
