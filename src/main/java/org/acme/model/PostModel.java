package org.acme.model;

import java.util.ArrayList;
import java.util.List;

public class PostModel {

	Long id;
	String title;
	String content;
	List<String> tags;
	
	public PostModel() {
		super();
		this.id = 0l;
		this.title = "";
		this.content = "";
		this.tags = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	
	
}
