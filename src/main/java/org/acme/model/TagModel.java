package org.acme.model;

import java.util.ArrayList;
import java.util.List;

public class TagModel {

	Long id;
	String label;
	List<PostModel> posts;
	
	public TagModel() {
		super();
		this.id = 0l;
		this.label = "";
		this.posts = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<PostModel> getPosts() {
		return posts;
	}

	public void setPosts(List<PostModel> posts) {
		this.posts = posts;
	}
	
}
